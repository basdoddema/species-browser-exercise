package nl.bioinf.bgdoddema.exercise2.servlets;

import nl.bioinf.bgdoddema.exercise2.WebConfig;
import nl.bioinf.bgdoddema.exercise2.model.DogCollection;
import nl.bioinf.bgdoddema.exercise2.model.Dog;
import nl.bioinf.bgdoddema.exercise2.model.HistoryManager;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SpeciesBrowserServlet", urlPatterns = "/home", loadOnStartup = 1)
public class SpeciesBrowserServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        templateEngine.process("speciesbrowser", ctx, response.getWriter());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        List<Dog> dogs = DogCollection.DOGS;
        ctx.setVariable("dogs", dogs);


        templateEngine.process("speciesbrowser", ctx, response.getWriter());
    }
}