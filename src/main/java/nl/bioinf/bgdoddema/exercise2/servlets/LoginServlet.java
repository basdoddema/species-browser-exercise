package nl.bioinf.bgdoddema.exercise2.servlets;

import nl.bioinf.bgdoddema.exercise2.WebConfig;
import nl.bioinf.bgdoddema.exercise2.model.Dog;
import nl.bioinf.bgdoddema.exercise2.model.DogCollection;
import nl.bioinf.bgdoddema.exercise2.model.Role;
import nl.bioinf.bgdoddema.exercise2.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        //fetch the session object
        //if it is not present, one will be created
        HttpSession session = request.getSession();
        String nextPage;
        List<Dog> dogs = DogCollection.DOGS;
        ctx.setVariable("dogs", dogs);


        if (session.isNew() || session.getAttribute("user") == null) {
            boolean authenticated = authenticate(username, password);
            if (authenticated) {
                session.setAttribute("user", new User("Henk", "henk@example.com", Role.USER));
                nextPage = "speciesbrowser";
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            nextPage = "speciesbrowser";
        }
        templateEngine.process(nextPage, ctx, response.getWriter());
    }

    private boolean authenticate(String username, String password) {
        return username.equals("Bas") && password.equals("wachtwoord");
    }

    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("message", "Fill out the login form");
        ctx.setVariable("message_type", "info");
        templateEngine.process("login", ctx, response.getWriter());
    }
}