package nl.bioinf.bgdoddema.exercise2.servlets;

import nl.bioinf.bgdoddema.exercise2.WebConfig;
import nl.bioinf.bgdoddema.exercise2.model.DogCollection;
import nl.bioinf.bgdoddema.exercise2.model.Dog;
import nl.bioinf.bgdoddema.exercise2.model.HistoryManager;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "SpeciesDetailServlet", urlPatterns = "/species.detail", loadOnStartup = 0)
public class SpeciesDetailServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("species_detail", ctx, response.getWriter());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String species = request.getParameter("species");
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        Map<String, Dog> dogs = DogCollection.DOGS_MAP;
        ctx.setVariable("scName", dogs.get(species).getScientificName());
        ctx.setVariable("name", dogs.get(species).getDutchName());
        ctx.setVariable("weight", dogs.get(species).getWeight());
        ctx.setVariable("image", dogs.get(species).getImage());

        HttpSession session = request.getSession();
        HistoryManager historyManager;
        if (session.isNew()) {
            historyManager = new HistoryManager();
            session.setAttribute("history", historyManager);
        } else {
            historyManager = (HistoryManager)session.getAttribute("history");
        }
        historyManager.addItem(species);
        ctx.setVariable("history", historyManager.getHistoryItems());
        templateEngine.process("species_detail", ctx, response.getWriter());

    }
}
