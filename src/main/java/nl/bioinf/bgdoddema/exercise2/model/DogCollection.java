package nl.bioinf.bgdoddema.exercise2.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class DogCollection {
    public static List<Dog> DOGS;
    public static Map<String, Dog> DOGS_MAP = new HashMap<>();


    static {
        DogCollection.DOGS = readIntoList();
        DogCollection.DOGS_MAP = readIntoMap();
    }

    public List<Dog> getDogs() {
        return Collections.unmodifiableList(DOGS);
    }

    public Map<String, Dog> getDogsMap() {
        return Collections.unmodifiableMap(DOGS_MAP);
    }


    public static List<Dog> readIntoList() {
        List<Dog> dogs = new ArrayList<>();

        try {
            BufferedReader csvReader = new BufferedReader(new FileReader("C:\\Users\\basdo\\Desktop\\IdeaProject\\species-browser-exercise\\data\\species.csv"));
            String row;
            int iteration = 0;
            while ((row = csvReader.readLine()) != null) {
                if(iteration == 0) {
                    iteration++;
                    continue;
                }
                String[] data = row.split(",");
                Dog d = new Dog(data[0],data[1],data[2],data[3],data[4]);
                dogs.add(d);

            }
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dogs;
    }

    public static Map<String, Dog> readIntoMap() {
        Map<String, Dog> dogs = new HashMap<>();

        try {
            BufferedReader csvReader = new BufferedReader(new FileReader("C:\\Users\\basdo\\Desktop\\IdeaProject\\species-browser-exercise\\data\\species.csv"));
            String row;
            int iteration = 0;
            while ((row = csvReader.readLine()) != null) {
                if(iteration == 0) {
                    iteration++;
                    continue;
                }
                String[] data = row.split(",");
                Dog d = new Dog(data[0],data[1],data[2],data[3],data[4]);
                dogs.put(data[1],d);

            }
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dogs;
    }

    public static void main(String[] args) {
        DogCollection reader = new DogCollection();
        List<Dog> dogs = DogCollection.DOGS;
        System.out.println(dogs);
    }
}
