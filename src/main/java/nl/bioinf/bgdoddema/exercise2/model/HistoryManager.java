package nl.bioinf.bgdoddema.exercise2.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class HistoryManager {
    public static int DEFAULT_MAXIMUMSIZE = 5;
    private int maxSize;
    private LinkedList<String> history = new LinkedList<>();


    public HistoryManager() {
        this.maxSize = DEFAULT_MAXIMUMSIZE;
    }

    public void addItem(String historyItem) {
        if (history.size() == maxSize) {
            history.remove(history.size() - 1);
        }
        history.add(0, historyItem);
    }

    public List<String> getHistoryItems() {
        return Collections.unmodifiableList(this.history);
    }

    @Override
    public String toString() {
        return "HistoryManager{" +
                "history=" + history.toString() +
                '}';
    }

    public static void main(String[] args) {
        HistoryManager hm = new HistoryManager();
        hm.addItem("test");
        System.out.println(hm);
        hm.addItem("test1");
        System.out.println(hm);
        hm.addItem("test3");
        System.out.println(hm);
        hm.addItem("test4");
        System.out.println(hm);
        hm.addItem("test5");
        System.out.println(hm);
        hm.addItem("test6");
        System.out.println(hm);
    }

}
