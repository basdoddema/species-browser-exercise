package nl.bioinf.bgdoddema.exercise2.model;

public class Dog {
    private String scientificName;
    private String englishName;
    private String dutchName;
    private String weight;
    private String image;

    public Dog(String scientificName, String englishName, String dutchName, String weight, String image) {
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.weight = weight;
        this.image = image;
    }


    public String getEnglishName() {
        return englishName;
    }

    public String getDutchName() {
        return dutchName;
    }

    public String getWeight() {
        return weight;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getImage() {return image; }

    @Override
    public String toString() { return scientificName  + ", " + englishName + ", " + dutchName + ", "
            + weight;
    }
}
