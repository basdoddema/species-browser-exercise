package nl.bioinf.bgdoddema.exercise2.model;

public enum Role {
    GUEST,
    USER,
    ADMIN;
}
